"use strict";
exports.__esModule = true;
var Table = /** @class */ (function () {
    function Table(rowCount) {
        this.inputArr = [];
        this.rowCount = rowCount;
    }
    Table.prototype.toHtml = function () {
        var table = document.createElement('table');
        var tableHead = this.createTableHead();
        var tableBody = this.createTableBody();
        //Adding table head and body to table
        table.appendChild(tableHead);
        table.appendChild(tableBody);
        return table;
    };
    Table.prototype.createTableHead = function () {
        var tableHead = document.createElement('thead');
        //Creating table header
        var headRow = document.createElement('tr');
        var headCell1 = document.createElement('td');
        var headCell2 = document.createElement('td');
        headCell1.innerHTML = 'Input';
        headCell2.innerHTML = 'Cube';
        //Building headRow
        headRow.appendChild(headCell1);
        headRow.appendChild(headCell2);
        //Appending headRow to tableHead
        tableHead.appendChild(headRow);
        return tableHead;
    };
    Table.prototype.createTableBody = function () {
        var tableBody = document.createElement('tbody');
        var refreshedTableBody = this.refreshTableBody(tableBody);
        return refreshedTableBody;
    };
    Table.prototype.refreshTableBody = function (tableBody) {
        var _this = this;
        var _loop_1 = function (i) {
            //Creating row
            var tr = document.createElement('tr');
            var floatInput = createElementWithId('input', "floatInput-".concat(i));
            floatInput.setAttribute('type', 'number');
            floatInput.placeholder = 'Please enter number';
            floatInput.addEventListener('change', function (e) { console.log('change'); _this.handleInput(e, i); });
            var outputP = createElementWithId('p', "outputP-".concat(i));
            if (this_1.inputArr[i]) {
                floatInput.value = this_1.inputArr[i].inVal;
                outputP.innerHTML = Math.pow(this_1.inputArr[i].inVal, 3);
            }
            var floatInputTd = createElementWithChild('td', '', floatInput);
            var cubeOutputTd = createElementWithChild('td', '', outputP);
            //Appending td to tr
            tr.appendChild(floatInputTd);
            tr.appendChild(cubeOutputTd);
            //Appending tr to tbody
            tableBody.appendChild(tr);
        };
        var this_1 = this;
        for (var i = 0; i < this.rowCount; i++) {
            _loop_1(i);
        }
        return tableBody;
    };
    Table.prototype.changeRowNum = function (num) {
        this.rowCount = num;
    };
    Table.prototype.handleInput = function (e, i) {
        var val = e.target.value;
        if (val) {
            this.inputArr.splice(i, 1, { inVal: val });
        }
        console.log('inputarr', this.inputArr);
    };
    Table.prototype.handleCalculate = function () {
        var table = document.getElementsByTagName('table')[0];
        var tbody = document.getElementsByTagName('tbody')[0];
        table.removeChild(tbody);
        var newTableBody = document.createElement('tbody');
        var refreshedTableBody = this.refreshTableBody(newTableBody);
        table.appendChild(refreshedTableBody);
    };
    return Table;
}());
exports["default"] = Table;
var mainTable = new Table(2);
function handleRowNumChange(val) {
    console.log('val', val);
    mainTable.inputArr = mainTable.inputArr.slice(0, val);
    mainTable.changeRowNum(val);
    var tableContainer = document.getElementById('tableContainer');
    var table = document.getElementsByTagName('table')[0];
    table && tableContainer.removeChild(table);
    tableContainer === null || tableContainer === void 0 ? void 0 : tableContainer.appendChild(mainTable.toHtml());
    console.log(mainTable);
    console.log(mainTable.toHtml());
}
var handleCalculate = function () { return mainTable.handleCalculate(); };
function createElementWithId(el, id) {
    var element = document.createElement(el);
    element.id = id;
    return element;
}
function createElementWithChild(parentType, parentId, child) {
    var parent = createElementWithId(parentType, parentId);
    parent.appendChild(child);
    return parent;
}
