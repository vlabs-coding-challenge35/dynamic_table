export default class Table {
	rowCount;
	inputArr=[];
	constructor(rowCount) {
		this.rowCount = rowCount;
	}

	toHtml(){
		const table = document.createElement('table');
		const tableHead = this.createTableHead();
		const tableBody = this.createTableBody();
		
		//Adding table head and body to table
		table.appendChild(tableHead);
		table.appendChild(tableBody);

		return table;
	}

	createTableHead(){
		const tableHead = document.createElement('thead');

		//Creating table header
		const headRow = document.createElement('tr');
		const headCell1 = document.createElement('td');
		const headCell2 = document.createElement('td');
		headCell1.innerHTML = 'Input';
		headCell2.innerHTML = 'Cube';
		
		//Building headRow
		headRow.appendChild(headCell1);
		headRow.appendChild(headCell2);

		//Appending headRow to tableHead
		tableHead.appendChild(headRow);

		return tableHead;
	}

	createTableBody(){
		const tableBody = document.createElement('tbody');
		const refreshedTableBody = this.refreshTableBody(tableBody);
		return refreshedTableBody;
	}

	refreshTableBody(tableBody){
		for (let i=0; i<this.rowCount; i++){
			//Creating row
			const tr = document.createElement('tr');
			const floatInput = createElementWithId('input', `floatInput-${i}`);

			floatInput.setAttribute('type', 'number');
			floatInput.placeholder = 'Please enter number';
			floatInput.addEventListener('change', (e)=>{console.log('change');this.handleInput(e, i)});
			const outputP = createElementWithId('p', `outputP-${i}`);

			if(this.inputArr[i]){
				floatInput.value = this.inputArr[i].inVal;
				outputP.innerHTML = this.inputArr[i].inVal ** 3;
			}
			
			const floatInputTd = createElementWithChild('td', '', floatInput);
			const cubeOutputTd = createElementWithChild('td', '', outputP);
			

			//Appending td to tr
			tr.appendChild(floatInputTd);
			tr.appendChild(cubeOutputTd);

			//Appending tr to tbody
			tableBody.appendChild(tr);
		}
		return tableBody;
	}


	changeRowNum(num){
		this.rowCount = num;
	}

	handleInput(e, i){
		const val = e.target.value;
		if(val){
			this.inputArr.splice(i, 1, {inVal:val});
		}
		console.log('inputarr', this.inputArr);
	}

	handleCalculate(){
		const table = document.getElementsByTagName('table')[0];
		const tbody = document.getElementsByTagName('tbody')[0];
		table.removeChild(tbody);

		const newTableBody = document.createElement('tbody');
		const refreshedTableBody = this.refreshTableBody(newTableBody);
		table.appendChild(refreshedTableBody);
	}
}

const mainTable = new Table(2);

function handleRowNumChange(val){
	console.log('val', val);
	mainTable.inputArr = mainTable.inputArr.slice(0, val);
	mainTable.changeRowNum(val);
	const tableContainer = document.getElementById('tableContainer');
	const table = document.getElementsByTagName('table')[0];
	table && tableContainer.removeChild(table);
	tableContainer?.appendChild(mainTable.toHtml());
	console.log(mainTable);
	console.log(mainTable.toHtml());
}

const handleCalculate = () => mainTable.handleCalculate();

function createElementWithId(el, id){
	const element = document.createElement(el);
	element.id = id;
	return element;
}

function createElementWithChild(parentType, parentId, child){
	const parent = createElementWithId(parentType, parentId);
	parent.appendChild(child);
	return parent;
}